# RIMBAUD.ZAP

«&nbsp;Il faut être absolument moderne.&nbsp;»

## Préambule

Dans le cadre de [l’Open Publishing Fest](https://openpublishingfest.org/), nous proposons dès le vendredi 22 mai 2020, de tenir une ZAP (Zone Autonome à Poétiser), d'installer un squat dans le monument Rimbaud et d'y lancer une réécriture frénétique et collective de sa *Saison en enfer*. Tout y sera amour et irrévérence.

<p class="pasdeligneblanche">Le texte de base d’Arthur Rimbaud, <em>Une saison en enfer</em> est également disponible dans son format de <a href="https://abrupt.ch/antilivre#microantilivre">microantilivre</a>&nbsp;:</p>

- <a href="https://txt.abrupt.ch/antilivre/microantilivre_abrupt_rimbaud_arthur_une_saison_en_enfer.pdf">la version PDF</a>
- <a href="https://txt.abrupt.ch/antilivre/microantilivre_abrupt_rimbaud_arthur_une_saison_en_enfer_imprimable.pdf">la version imprimable DIY</a>
- <a href="https://txt.abrupt.ch/antilivre/microantilivre_abrupt_rimbaud_arthur_une_saison_en_enfer_ebook.epub">la version ebook</a>


## Proposition

- Nous vous proposons d’écrire un livre collectif.
- Nous vous proposons de ne pas le signer.
- Nous vous proposons de le placer dans le domaine public volontaire.
- Nous vous proposons d’utiliser exclusivement des outils libres pour le réaliser.
- Nous vous proposons de revivre *Une saison en enfer*, de sonder le fantôme d’Arthur Rimbaud.
- Nous vous proposons de faire d’un monument un squat, de transformer la liturgie littéraire en une fête païenne.
- Nous vous proposons de placer nos technologies de l’information en «&nbsp;l’alchimie du verbe&nbsp;».
- Nous vous proposons de nous servir de Git et de Gitter pour installer une ZAP (Zone Autonome à Poétiser).

Toute création est une recréation, toute invention est un vol.

Ensemble, créons, recréons, inventons et volons.

Ensemble, nous sommes Arthur Rimbaud.

Ensemble, nous sommes absolument modernes.

## Participation

Comment participer ? Comment discuter ? Comment transmettre un texte ? Plusieurs possibilités :

- participer au [chat](https://gitter.im/antilivre/rimbaud.zap)
- utiliser la section *issues* comme un [forum](https://gitlab.com/antilivre/rimbaud.zap/issues)
- modifier directement le [texte](https://gitlab.com/antilivre/rimbaud.zap/blob/master/enfer.md) (il y a un bouton *Web IDE* pour ça avec une option de prévisualisation du format Markdown)
- pour les personnes familières de Git, cloner le dépôt et envoyer un *merge request*
- pour les personnes ne souhaitant pas se créer de compte, nous envoyer votre contribution par email (ecrire /arobase/ abrupt /point/ ch)

L'aventure se déroule dans ce le fichier [enfer.md](enfer.md).

Il est formaté selon le langage [Markdown](https://fr.wikipedia.org/wiki/Markdown).

Les mots, qui ne sont pas d'Arthur, sont indiqués en italiques (comme signalé ci-dessous, on n'y touche pas). Ces passages se trouvent, dans le fichier Markdown, entre astérisques (`*italique*` donne *italique*).

(Si vous souhaitez vous familiariser avec l'outil libre [Git](https://fr.wikipedia.org/wiki/Git), il existe une documentation [officielle](https://git-scm.com/book/fr/v2), ainsi qu'une grande quantité de tutoriels sur les Internets.)

## Règles

Trois règles :

1. Comme dans le monde du graff, pas de *toying* ! On ne touche pas au désordre... on ne fait qu'ajouter du chaos au chaos. De manière pratique, les modifications ne visent que les parties qui ne sont pas en italiques, et les ajouts se glissent avant ou après les contributions en italiques déjà présentes.
2. Tenez-vous bien !
3. Ne respectez pas la règle 2.

(Modération mode Dzerjinski : *sommeil*.)

## La licence

Ce projet est dédié au domaine public. Les textes et le code (ne faisant pas partie d’une librairie préexistante) sont mis à disposition selon les termes de la Licence Creative&nbsp;Commons&nbsp;Zero (CC&nbsp;1.0 Universel) — voir le fichier [LICENSE](LICENSE) à cet effet, ainsi que notre [politique](https://abrupt.ch/partage/) de partage.

## Z comme Zbeul

(Cette ZAP se tiendra jusqu’à son *actualisation*.)

(Rimbaud est une ZAP.)

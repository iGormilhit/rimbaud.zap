// Scripts
// Menu
const menuBtn = document.querySelector('.btn--menu');
const menu = document.querySelector('.menu');

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menu.classList.toggle('show');
});

// Markdown
let markdownTexts = document.querySelectorAll('.markdown');
markdownTexts.forEach(el => {
  const reader = new commonmark.Parser({smart: true});
  const writer = new commonmark.HtmlRenderer({sourcepos: true});
  const parsed = reader.parse(el.textContent);
  el.innerHTML = writer.render(parsed);
});

// Dark mode
const btnDark = document.querySelector('.btn--dark');

let darkModeToggle = false;
const darkModeTheme = localStorage.getItem('theme');

if (darkModeTheme) {
  document.documentElement.setAttribute('data-theme', darkModeTheme);

  if (darkModeTheme === 'dark') {
    darkModeToggle = true;
  }
}

function darkModeSwitch() {
  if (!darkModeToggle) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
    darkModeToggle = true;
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
    darkModeToggle = false;
  }
}

btnDark.addEventListener('click', darkModeSwitch);

// Baffle
let baffleTxt = baffle('h1')
                  .reveal(1000)
                  .set({
                    characters: 'RIMBAUD.ZAP'
                  });

// Change color
const text = document.querySelector('.enfer');
const rangeFg = document.querySelector('.range--fg');
const rangeEcriture = document.querySelector('.range--ecriture');
rangeFg.addEventListener("change", () => {
  document.documentElement.style.setProperty('--color-fg', 'rgba(var(--color-alpha-fg),'+ rangeFg.value / 100 + ')');
  });
rangeEcriture.addEventListener("change", () => {
  document.documentElement.style.setProperty('--color-ecriture', 'rgba(var(--color-alpha-ecriture),'+ rangeEcriture.value / 100 + ')');
  });
